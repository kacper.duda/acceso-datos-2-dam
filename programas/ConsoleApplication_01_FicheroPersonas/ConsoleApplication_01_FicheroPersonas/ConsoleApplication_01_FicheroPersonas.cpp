// ConsoleApplication_01_FicheroPersonas.cpp: define el punto de entrada de la aplicaci�n de consola.

/*
- Gestionar datos alumno:
� Nombre (40 caracteres)
� Edad
M�ximo 10 alumnos

OPCIONES:
1. Cargar datos
2. Escribir datos
3. Insertar datos en la tabla
4. Mostrar tabla en pantalla
5. Borrar datos de la tabla
6. Salir
*/

#include "stdafx.h"
#include <stdio.h>
#include <string.h>

/* ============================================================== */
/*           Declaraci�n de la estructura de alumnos			  */
/* ============================================================== */
struct TAlumno
{
	char nombre[40];
	int edad;
};

/* ============================== */
/*           Funciones			  */
/* ============================== */
void cargarDatos(char nombreFichero[50], struct TAlumno tablaAlumnos[10], int *numero)
{
	FILE *pf;
	char nombreAlumno[40];
	int	edadAlumno,
		posicion;

	posicion = 0;
	*numero = 0;
	pf = fopen(nombreFichero, "rt");
	if (pf != NULL)
	{
		while (!feof(pf)) // File End Of File - FEOF --> hasta que no llegue al final del fichero que siga leyendo
		{
			fscanf(pf, "%s", nombreAlumno);
			fscanf(pf, "%i", &edadAlumno);
			strcpy(tablaAlumnos[posicion].nombre, nombreAlumno); // se utiliza strcpy porque no es una varible inmediata
			tablaAlumnos[posicion].edad = edadAlumno;
			posicion++;
		}
		(*numero = posicion);
		fclose(pf);
	}
};

void guardarDatos(char nombreFichero[50], struct TAlumno tablaAlumnos[10], int numero)
{
	FILE *pf;
	int posicion;

	pf = fopen(nombreFichero, "wt");
	if (pf != NULL)
	{
		for (posicion = 0; posicion < numero; posicion++)
		{
			fprintf(pf, "%s\n", tablaAlumnos[posicion].nombre);
			fprintf(pf, "%i\n", tablaAlumnos[posicion].edad);
		}
		fclose(pf);
	}
}

void mostrarTabla(char nombreFichero[50], struct TAlumno tablaAlumnos[10], int numero) {
	int posicion;

	printf("\n\n");
	for (posicion = 0; posicion < numero; posicion++)
	{
		printf("\t%s - ", tablaAlumnos[posicion].nombre);
		printf("%i\n", tablaAlumnos[posicion].edad);
	}
	printf("\n\n");
}

void imprimirMenu()
{
	printf("MENU DE OPCIONES \n");
	printf("---------------- \n");
	printf("	1. Cargar datos del fichero \n");
	printf("	2. Guardar datos en el fichero \n");
	printf("	3. Mostrar tabla por pantalla \n");
	printf("	4. Insertar datos en la tabla \n");
	printf("	5. Borrar de la tabla \n");
	printf("	6. Salir \n");
	printf(" Introduce la opcion: ");
}

/* ====================================== */
/*           Programa principal			  */
/* ====================================== */
int main()
{
	int opcion;
	struct TAlumno tablaAlumnos[10];
	int numeroAlumnos = 0;

	do
	{
		imprimirMenu();
		scanf("%i", &opcion);
		switch(opcion)
		{
			case 1:
				cargarDatos("clase.txt", tablaAlumnos, &numeroAlumnos);	
				break;
			case 2:
				guardarDatos("clase2.txt", tablaAlumnos, numeroAlumnos);
				break;
			case 3:
				mostrarTabla("clase2.txt", tablaAlumnos, numeroAlumnos);
				break;
		}
	} while (opcion != 6);

	return 0;
}